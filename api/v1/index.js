// To expose all the routes
const payment = require('./routes/payment');

module.exports = {
  payment,
};

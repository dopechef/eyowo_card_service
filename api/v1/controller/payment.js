//Payment controller
const axios = require('axios');
//const request = require(request);
const mongoose = require('mongoose');

const pay = async (req, res) => {
    const { status } = req.params;
    switch (status) {
        case 'init':
            axios.post('https://c2.operapay.com/api/gateway/create', {
                "input": {
                    currency: "NGN",
                    publicKey: "5b06cc26fc1d0b00014364ce",
                    amount: "100",
                    reference: mongoose.Types.ObjectId().toString(),
                    countryCode: "NG",
                    tokenize: false,
                    instrumentType: "card",
                    cardNumber: req.body.cardNumber,
                    cardDateMonth: req.body.cardDateMonth,
                    cardDateYear: req.body.cardDateYear,
                    cardCVC: req.body.cardCVC,
                }
            })
                .then(async  response => {
                    const data = response.data;

                    const r = await commit(data.data.gatewayCreate.token)
                    const rdata = r.data
                    res.status(200).json({ success: true, message: 'Transaction Initialized Successfully', rdata });
                })
                .catch(err => {
                    //console.log(err)
                    res.status(400).json({ success: false, message: 'Transaction Init failed', err })
                });
            break;
        case 'input-otp':
            axios.post('https://c2.operapay.com/api/gateway/input-otp',
                { "token": req.body.token, "otp": req.body.otp }
            ).then(resp => {
                const data = resp.data;
                res.status(200).json({ success: true, message: 'Transaction Initialized Successfully', data });
            }).catch(err => {
                res.status(400).json({ success: false, message: 'Transaction Init failed', err })
            })
            break;
        case 'input-pin':
            axios.post('https://c2.operapay.com/api/gateway/input-pin', {
                "token": req.body.token,
                "pin": req.body.pin
            })
                .then(resp => {
                    const data = resp.data
                    res.status(200).json({ success: true, message: 'Transaction Initialized Successfully', data });
                })
                .catch(err => {
                    res.status(400).json({ success: false, message: 'Transaction Init failed', err })
                })
            break;
        default:
            res.status(200).json({ success: true, message: 'Endpoint working' });
    }
}



const commit = async (token) => {
    return axios.post('https://c2.operapay.com/api/gateway/commit', {

        input: {
            "currency": "NGN",
            "privateKey": "c0942a28b645b2b0de5303e40524ee75",
            "amount": "100",
            "countryCode": "NG",
            "token": token,
        }

    })
        .then(response => {
            const data = response.data
            return ({ success: true, message: 'Transaction Committed Successfully', data });
        })
        .catch(err => {
            return ({ success: false, message: 'Transaction Commit failed', err })
        })
}
module.exports = {
    pay,
}
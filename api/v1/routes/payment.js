let express = require('express')
let PaymentController = require('../controller/payment')
var api = express.Router()

api.post('/v1/eyowo/payment/:status', (req,res) => {
    PaymentController.pay(req,res)
});

module.exports = api;
